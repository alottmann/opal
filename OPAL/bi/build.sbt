name := "Bytecode Infrastructure"

version := "0.8.0"

scalacOptions in (Compile, doc) := Opts.doc.title("OPAL - Bytecode Infrastructure") 
